<?php
/**
 * Utilisations de pipelines par IPs pour les auteurs
 *
 * @plugin     IPs pour les auteurs
 * @copyright  2020
 * @author     marcimat
 * @licence    GNU/GPL
 * @package    SPIP\Ipset\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Actualiser le cache des IPs lorsqu’un auteur est mis à jour
 *
 * @param array $flux
 * @return array
 */
function ipset_post_edition($flux) {

	if (
		$flux['args']['table'] === 'spip_auteurs'
		and $id_auteur = $flux['args']['id_objet']
		and isset($flux['data']['access_ips'])
		and $flux['args']['action'] === 'modifier'
	) {
		include_spip('inc/ipset');
		ipset_update_ips_cache();
	}

	return $flux;
}

/**
 * Ajoute un lien vers la page de liste des auteurs avec ips sur les pages auteurs et visiteurs.
 *
 * @param array $flux
 * @return array
 */
function ipset_affiche_gauche($flux) {
	if (
		in_array($flux['args']['exec'], ['auteurs', 'visiteurs'])
		and autoriser('voir', '_auteurs')
	) {
		$flux['data'] .= recuperer_fond('prive/squelettes/inclure/auteurs_access_ips');
	}
	return $flux;
}