# IPSet

IPSet est un plugin pour SPIP qui permet de définir des IPs ou Plages d’IPs à des auteurs SPIP. 
Il utilise la librairie éponyme [IPSet de Wikimedia](https://github.com/wikimedia/IPSet).

## Principe général

Le plugin permet de tester rapidement qu’une IP entrante fait partie d’une liste d’IPs ou de Plages d’IPs connues
du site, et éventuellement de retrouver à quel auteur SPIP cette IP est définie.
On s’appuie essentiellement sur 2 fonctions PHP de base :

- `ipset_get()` retourne un IPSet avec l’ensemble des plages / IPs connues, que l’on peut tester avec l’IP du visiteur :

```php
$ip = $GLOBALS['ip'];
$ipset = ipset_get();
if ($ipset->match($ip)) {
    // On connait cette IP
}
```

- `ipset_access_author()` va plus loin et retourne d’id_auteur éventuel associé à l’IP du visiteur, 
si on connait cette IP. Dans ce cas, une session anonyme est créé contenant les entrées `ip_id_auteur` et `ip_name`
renseignées, qui correspondent à l’id_auteur et nom de l’auteur associé.


## Fonctionnement technique

### Champ `access_ips`

Pour stocker les IPs / Plages d’IPs associées aux auteurs, le plugin ajoute un champ `access_ips` à la table `spip_auteurs` de SPIP.
Une saisie dans le formulaire d’auteurs permet de renseigner ce champ. 
On peut définir une ou plusieurs adresses IPs ou plages d’IPs (séparées par des virgules),
tel que :

- `123.123.123.1, 123.123.123.2`
- `123.123.123.0/28`

Les plages d’IPs sont à fournir en [notation CIDR](https://fr.wikipedia.org/wiki/CIDR).

Une vérification empêche d’utiliser deux fois la même IP ou plage d’IP sur différents auteurs.


### Méta `ipset`

À chaque actualisation d’une IP dans le formulaire d’édition d’auteur, un cache méta (dans `spip_meta`) est mis à jour
avec une définition IPSet de l’ensemble des IPs/Plages définies pour les auteurs du site.

Au besoin, on peut vérifier rapidement qu’une IP visiteur du site fait partie de ce set d’IPs défini, en appelant
la fonction `ipset_get()` qui retourne l’IPSet depuis ce cache méta (ou le calcule s’il n’existe pas).

### Cache disque `tmp/accessip`

Lorsqu’on a besoin d’associer une IP à un auteur (par exemple pour afficher son nom dans le header du site ou
pour des autorisations spécifiques), le calcul pour retrouver à quel auteur est associé une IP est plus gourmand
(on doit recalculer un IPSet avec les IPs/Plages d’IPs de chaque auteur possible et le tester avec l’IP visiteur).

Pour éviter ces calculs, on stocke l’association dans un cache disque lorsque l’on trouve qu’une IP est associée à un auteur
Cela se fait par exemple lorsqu’on utilise la fonction `ipset_access_author()` :

- s’il n’y a pas de session anonyme déjà pour cette IP, on teste
- si l’IP est connue (l’IPSet global est validé)
- si oui, on appelle `ipset_get_author($ip)` qui retourne l’id_auteur, en le cherchant soit en cache disque (dans `tmp/accessip/$ip`) 
 soit en le calculant et en créant ce cache.
- puis on crée une session anonyme pour ce visiteur.

### Cache disque `tmp/noflood`

En même temps que le cache `tmp/accessip`, l’IP est ajouté dans `tmp/noflood/$ip` ce qui peut servir à se faire
whitelister par des antispams. 

### Génie `accessip`

Un génie nettoie périodiquement les caches disques `tmp/accessip` et `tmp/noflood` des fichiers plus vieux que 7 jours.


## Exemples d’utilisation

### Autorisation

Vérifier qu’on connait l’auteur : il est identifié ou a une IP connue :

```php
function autoriser_exemple_acceder_dist($faire, $type, $id, $qui, $opt) {
    if ((!$qui or !$qui['id_auteur']) and !ipset_access_author()) {
        return false;
    }
    // ...
    return true;
}
```

### Nom du visiteur en squelettes

```html
<div class="small">
    <?php if (!isset($GLOBALS['visiteur_session']['statut'])) { ?>
        <?php if (ipset_access_author()) { ?>
            <span class="localisation">
                <i class="picto picto-user"></i><span> <?php echo $GLOBALS['visiteur_session']['ip_name']; ?></span>
            </span>
        <?php }  ?>
        <a href="#URL_PAGE{login}" class="link-popin link-popin-login"><i class="picto picto-login"></i><span> <:exemple:lien_connexion:></span></a>
    <?php }  else { ?>
        <a href="#URL_PAGE{compte}"><i class="picto picto-user"></i><span> <:exemple:lien_compte:></span></a>
    <?php } ?>
</div>
```