<?php

/**
 * Gestion du nettoyage des access IP
 * @plugin     IPs pour les auteurs
 * @copyright  2020
 * @author     marcimat
 * @licence    GNU/GPL
 * @package    SPIP\Ipset\Genie
 **/

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}


/**
 * Cron de nettoyage des accès IP
 *
 * @param int $t
 *     Timestamp de la dernière exécution de cette tâche
 * @return int
 *     Positif si la tâche a été terminée, négatif pour réexécuter cette tâche
 **/
function genie_ipset_accessip_dist($t) {

	$ips = accessip_lister_vieux_access(_DIR_TMP . 'accessip', 7 * 24 * 3600);
	if ($ips) {
		spip_log('Nettoyage de ' . count($ips) . ' acces par IPs : ' . implode(', ', $ips), 'ipset');
		include_spip('inc/ipset');
		foreach ($ips as $ip) {
			$accessip = new AccessIP($ip);
			$accessip->remove();
		}
	}

	return 1;
}



/**
 * Nettoyer un répertoire suivant l'age de ses fichiers
 *
 * @param string $repertoire
 *     Répertoire à nettoyer
 * @param int $age_max
 *     Age maximum des fichiers en seconde. Par défaut 24*3600
 * @return array Liste des IPs trop vieilles
 **/
function accessip_lister_vieux_access($repertoire, $age_max = 86400) {

	$repertoire = rtrim($repertoire, '/');
	if (!is_dir($repertoire)) {
		return [];
	}

	$fichiers = scandir($repertoire);
	if ($fichiers === false) {
		return [];
	}

	$fichiers = array_diff($fichiers, ['..', '.', '.ok']);
	if (!$fichiers) {
		return [];
	}

	$ips = [];
	foreach ($fichiers as $fichier) {
		$chemin = $repertoire . DIRECTORY_SEPARATOR . $fichier;
		if (is_file($chemin) and !jeune_fichier($chemin, $age_max)) {
			$ips[] = $fichier;
		}
	}

	return $ips;
}

