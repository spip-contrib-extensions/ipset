<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Une ou plusieurs adresses IP pour accéder au contenu
 * utilisé par les auteurs (genre bibliothèque)
 *
 * Formats acceptés : IPv4 ou IPv6, séparées par des virgules
 * une IP ne peut être affectée qu'à un seul auteur à la fois
 *
 * @param string $valeur
 *   La valeur à vérifier.
 * @param array $options
 * @return string
 *   Retourne une chaine vide si c'est valide, sinon une chaine expliquant l'erreur.
 */
function verifier_access_ips_dist($valeur, $options = array()) {

	if (!is_string($valeur)) {
		return _T('ipset:erreur_format_ip_string');
	}

	// crade
	$id_auteur = intval(_request('id_auteur'));

	$ips = explode(',', $valeur);
	$ips = array_map('trim', $ips);
	$ips = array_filter($ips);

	// Charger IPTools…
	include_spip('lib/IPTools/src/PropertyTrait');
	include_spip('lib/IPTools/src/IP');
	include_spip('lib/IPTools/src/Range');
	include_spip('lib/IPTools/src/Network');

	foreach ($ips as $ip) {

		// Tester la validité de la définition, qui peut être une IP ou un CIDR (IP+plage)
		try {
			$network = \IPTools\Network::parse($ip);
			// verifier l’ip (Network est trop tolérant). Si plage, on ne prend que le début
			$_ip = $ip;
			if (false !== $i = strpos($ip, '/')) {
				$_ip = substr($ip, 0, $i);
			}
			if (false === @inet_pton($_ip)) {
				throw new \Exception(sprintf("Invalid IP address : %s", $ip));
			}
		} catch (Exception $e) {
			return _T('ipset:erreur_format_ip', array('ip' => $ip));
		}

		// si cette IP est deja utilisee ?
		$deja_auteurs = sql_allfetsel('access_ips, id_auteur','spip_auteurs','id_auteur != ' . intval($id_auteur).' AND access_ips LIKE '.sql_quote("%$ip%"));
		foreach ($deja_auteurs as $auteur) {
			$deja_ips = explode(',', $auteur['access_ips']);
			$deja_ips = array_map('trim', $deja_ips);
			if (in_array($ip, $deja_ips)) {
				return _T('ipset:erreur_ip_deja_utilisee', array('ip' => $ip, 'id' => $auteur['id_auteur']));
			}
		}
	}

	return ''; // pas d'erreur
}
