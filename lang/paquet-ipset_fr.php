<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'ipset_description' => 'Permet d’attribuer une ou plusieurs IPs ou plages d’IPs à des auteurs SPIP.',
	'ipset_nom' => 'IPs pour les auteurs',
	'ipset_slogan' => 'Permet d’affecter des IPs à des auteurs',
);
