<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'auteurs_access_ips' => 'Auteurs avec accès par IP',
	'auteurs_access_ips_info' => 'Auteurs ayant au moins une IP renseignée dans le champ dédié.',

	// E
	'erreur_format_ip_string' => 'Format de valeur invalide (string)',
	'erreur_format_ip' => '<code>@ip@</code> n\'est pas une adresse IP valide',
	'erreur_ip_deja_utilisee' => '<code>@ip@</code> est déjà affectée à l\'auteur #@id@',

	// I
	'ipset_titre' => 'IPs pour les auteurs',
	'ip_un' => '1 IP',
	'ip_nb' => '@nb@ IPs',

	// L
	'label_access_ips' => 'Accès par IPs',
	'label_access_ips_explication' => 'Liste d’IPs ou de plages d’IPs (CIDR) séparés par des virgules.',

	// T
	'ipset_diagnostic' => 'Diagnostic IP',
	'ipset_diagnostic_info' => 'Cette page affiche les @nb@ dernières visites sur la page : ',
);
