<?php
/**
 * Options au chargement du plugin IPs pour les auteurs
 *
 * @plugin     IPs pour les auteurs
 * @copyright  2020
 * @author     marcimat
 * @licence    GNU/GPL
 * @package    SPIP\Ipset\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Retourne un IPSet : pour vérification rapide d’une IP sur les ranges d’ip des auteurs.
 * @return \Wikimedia\IPSet
 */
function ipset_get(): \Wikimedia\IPSet {
	/** @var $ipset \Wikimedia\IPSet */
	static $ipset = null;

	if (!is_null($ipset)) {
		return $ipset;
	}

	// Retourner le cache des IPs compilé si déjà calculé.
	// Selon la méthode de cache, la classe IPSet doit être connue !
	if (!defined('_VAR_MODE') or _VAR_MODE !== 'recalcul') {
		if ($ipset = ipset_get_cache()) {
			return $ipset;
		}
	}

	spip_log("Cache ipset absent -> calcul", 'ipset');
	include_spip('inc/ipset');
	return ipset_update_ips_cache();
}

/**
 * Retourne le cache meta IPSet global
 * @return \Wikimedia\IPSet
 */
function ipset_get_cache(): ?\Wikimedia\IPSet  {
	include_spip('inc/config');
	include_spip('lib/IPSet/src/Wikimedia/IPSet');
	$ipset = lire_config('ipset');
	if (
		$ipset
		and $ipset = @unserialize(base64_decode($ipset))
		and $ipset instanceof \Wikimedia\IPSet
	) {
		return $ipset;
	}
	return null;
}

/**
 * Verifier et retrouver id_auteur lie a une IP, si besoin
 * avec memo en session
 * @return bool|int Id auteur, sinon false.
 */
function ipset_access_author(): ?int {

	if (isset($GLOBALS['visiteur_session']['ip_id_auteur'])){
		return $GLOBALS['visiteur_session']['ip_id_auteur'];
	}

	$ip = $GLOBALS['ip'];
	$ipset = ipset_get();
	if ($ipset->match($ip)) {
		spip_log("IP : acceptée", "ipset");
		// a partir du moment ou on match, il faut enregistrer le resultat en session pour pas faire un acces SQL a chaque hit
		// l’ip peut matcher un range d’ip… et retrouver l’auteur est fastidieux.
		include_spip('inc/ipset');
		$id_auteur = ipset_get_author($ip);
		if (!function_exists('session_set')) {
			include_spip('inc/session');
		}
		session_set("ip_id_auteur", $id_auteur);
		// retrouver le nom de l’auteur
		if ($id_auteur) {
			include_spip('base/abstract_sql');
			$nom = sql_getfetsel('nom', 'spip_auteurs', array('id_auteur = ' . $id_auteur));
			session_set("ip_name", $nom);
			spip_log("IP : Auteur $id_auteur / Nom : $nom", "ipset");
		} else {
			spip_log("IP : Pas d’auteur !", "ipset." . _LOG_ERREUR);
			session_set("ip_name", null);
		}
		return $id_auteur;
	}

	return null;
}
