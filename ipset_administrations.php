<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin IPs pour les auteurs
 *
 * @plugin     IPs pour les auteurs
 * @copyright  2020
 * @author     marcimat
 * @licence    GNU/GPL
 * @package    SPIP\Ipset\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'installation et de mise à jour du plugin IPs pour les auteurs.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function ipset_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	// ajout du champ compte_institution_ips + disclaimer_articles
	include_spip('inc/cextras');
	include_spip('base/ipset');
	$champs = ipset_declarer_champs_extras();
	cextras_api_upgrade($champs, $maj['create']);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin IPs pour les auteurs.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function ipset_vider_tables($nom_meta_base_version) {
	include_spip('inc/cextras');
	include_spip('base/ipset');
	cextras_api_vider_tables(ipset_declarer_champs_extras());
	effacer_meta($nom_meta_base_version);
}
