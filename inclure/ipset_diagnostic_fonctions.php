<?php

/**
 * Génère et loggue différentes infos sur l’IP, les Caches IPSet pour ce visiteur...
 * @return array
 */
function ipset_diagnostic_collect() {

	spip_log("Visite IP diagnostic", 'ipset');

	$data = array();
	$ip = $GLOBALS['ip'];

	$data['Your IP'] = $ip;
	$data['Cache IPset exists'] = ipset_get_cache() ? 'yes' : 'no';
	$data['Your Cache Session exists'] = isset($GLOBALS['visiteur_session']['ip_id_auteur']) ? 'yes' : 'no';
	if (($data['Your Cache Session exists'] === 'yes') and empty($GLOBALS['visiteur_session']['ip_id_auteur'])) {
		$data['Your Cache Session exists'] .= " (but empty!)";
	}
	$data['Your IP Session is accepted'] = ipset_access_author() ? 'yes' : 'no';
	$data['Your name'] = !empty($GLOBALS['visiteur_session']['ip_name']) ? $GLOBALS['visiteur_session']['ip_name'] : 'unknown';

	$ipset = ipset_get();
	$data['IP matched the IPSet'] = $ipset->match($ip) ? 'yes' : 'no';

	include_spip('inc/ipset');
	$accessip = new AccessIP($ip);
	$data['IP found in file cache'] = $accessip->read() ? 'yes' : 'no';
	$data['IP found by full search'] = $accessip->search() ? 'yes' : 'no';

	// maintenant, on tente de corriger si problème de cache…
	if (!ipset_get_cache()) {
		$data['Error : Cache is missing'] = "We make it again...";
		ipset_update_ips_cache();
		if (!ipset_get_cache()) {
			$data['Error : Cache still missing'] = ":-(";
		}
	}

	$url = spip_htmlspecialchars($GLOBALS['meta']['adresse_site']);
	$data['Thank you'] =
		"Thank you for visiting this page. It will help us improve the reliability of your access."
		. " <br />You can now return on the home page: <a href='$url'>$url</a>";

	spip_log("Fin visite IP diagnostic", 'ipset');

	$data_public = $data;

	// Not visible to humans ...
	$data['Your ID Author'] = !empty($GLOBALS['visiteur_session']['ip_id_auteur']) ? $GLOBALS['visiteur_session']['ip_id_auteur'] : 'unknown';

	spip_log('IP DIAGNOSTIC', 'ipset_diagnostic');
	spip_log($data, 'ipset_diagnostic');

	return $data_public;
}