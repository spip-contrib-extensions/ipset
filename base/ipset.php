<?php
/**
 * Fichier déclarant les champs de base de données
 *
 * @plugin     IPs pour les auteurs
 * @copyright  2020
 * @author     marcimat
 * @licence    GNU/GPL
 * @package    SPIP\Ipset\Base
 */

/**
 * Champs extra sur auteurs
 */
function ipset_declarer_champs_extras($champs = array()){
	$champs['spip_auteurs']['access_ips'] = array(
		'saisie' => 'access_ips', //Type du champs (voir plugin Saisies)
		'options' => array(
			'nom' => 'access_ips',
			'label' => _T('ipset:label_access_ips'),
			'sql' => "mediumtext DEFAULT '' NOT NULL", // declaration sql
			'defaut' => '',
			'explication' => _T('ipset:label_access_ips_explication'),
		),
		'verifier' => array(
			'type' => 'access_ips'
		),
	);
	return $champs;
}

