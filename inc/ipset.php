<?php

/**
 * Fonctions du plugin IPs pour les auteurs
 *
 * @plugin     IPs pour les auteurs
 * @copyright  2020
 * @author     marcimat
 * @licence    GNU/GPL
 * @package    SPIP\Ipset\Fonctions
 */

include_spip('inc/AccessIP');

/**
 * Retourne la liste des IP / plages d’IP des auteurs avec l’id_auteur associé en clé
 *
 * @return array [ id_auteur => [ liste ip ou plages ip ]]
 */
function ipset_get_all_ips_by_authors() {
	static $couples = null;

	if (is_array($couples)) {
		return $couples;
	}

	include_spip('base/abstract_sql');

	// Chaque entrée est une liste séparée par des virgules d’IPs ou de CIDR
	$couples = sql_allfetsel(
		'id_auteur, access_ips',
		'spip_auteurs',
		'statut!='.sql_quote('5poubelle').' AND access_ips!=\'\''
	);
	$couples = array_column($couples, 'access_ips', 'id_auteur');
	$couples = array_map('ipset_filter_access_ips', $couples);
	return $couples;
}

/**
 * Découpe une chaîne d’ips en tableau
 * @param string $ips Liste d’ips et plages, séparés par ','
 * @return array Liste d’ips et plage
 */
function ipset_filter_access_ips(string $ips) : array {
	$ips = explode(',', $ips);
	$ips = array_map('trim', $ips);
	$ips = array_filter($ips);
	return array_unique($ips);
}

/**
 * Retourne la liste des IP / plages d’IP de tous les auteurs
 *
 * @uses ipset_get_all_ips_by_authors()
 * @return array
 */
function ipset_get_all_ips() {
	$relations = ipset_get_all_ips_by_authors();
	// combine tous les sous tableaux d’IP en 1 seul…
	$ips = array_reduce($relations, function ($carry, $item) {
		return array_merge($carry, $item);
	}, []);
	return $ips;
}


/**
 * Pour eviter un acces en base a chaque hit on compile les IPs dans une meta
 *
 * @return \Wikimedia\IPSet
 */
function ipset_update_ips_cache() : \Wikimedia\IPSet {
	// Calcul du cache des range d’IPs pour calcul de match global assez rapide.
	$ips = ipset_get_all_ips();
	spip_log("Mise en cache de " . count($ips) . " ips ou plages d’ips", 'ipset');
	spip_timer('ips');
	include_spip('lib/IPSet/src/Wikimedia/IPSet');
	$ipset = new \Wikimedia\IPSet($ips);
	ecrire_config("ipset", base64_encode(serialize($ipset)));
	spip_log("IPSet calculé en " . spip_timer('ips'), 'ipset');
	return $ipset;
}


/**
 * Retrouver l'id_auteur qui correspond a une IP (si il y en a un)
 *
 * @param string $ip
 * @return int|bool
 */
function ipset_get_author($ip) {
	static $ips = [];

	if (isset($ips[$ip])) {
		return $ips[$ip];
	}
	$accessip = new AccessIP($ip);
	$id_auteur = $accessip->author();
	if ($id_auteur) {
		$ips[$ip] = $id_auteur;
		return $id_auteur;
	}

	return false;
}
