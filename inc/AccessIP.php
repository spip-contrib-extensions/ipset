<?php

/**
 * Fonctions du plugin IPs pour les auteurs
 *
 * @plugin     IPs pour les auteurs
 * @copyright  2020
 * @author     marcimat
 * @licence    GNU/GPL
 * @package    SPIP\Ipset\Fonctions
 */


class AccessIP {
	/**
	 * @var string IP to test
	 */
	private $ip;
	/**
	 * @var string Cache directory for IP -> Id author
	 */
	private $dir_accessip = _DIR_TMP . 'accessip' . DIRECTORY_SEPARATOR;
	/**
	 * @var string Noflood directory for IPs (whitelist for NoSpam plugin and other)
	 */
	private $dir_noflood = _DIR_TMP . 'noflood' . DIRECTORY_SEPARATOR;

	public function __construct(string $ip) {
		$this->ip = $ip;
	}

	/**
	 * Find id_author for this IP in cache, else in database
	 *
	 * Touch cache files (accessip and noflood) to update time if author is found.
	 *
	 * @return int id_auteur
	 */
	public function author() : int {
		if ($id_auteur = $this->read()) {
			$this->touch();
			return $id_auteur;
		}
		if ($id_auteur = $this->search()) {
			$this->save($id_auteur);
			return $id_auteur;
		}
		return 0;
	}

	/**
	 * Read cache file for this IP, if exists
	 * @return int id_auteur
	 */
	public function read() : int {
		if (lire_fichier($this->dir_accessip . $this->ip, $id_auteur) and $id_auteur) {
			return (int)$id_auteur;
		}
		return 0;
	}

	/**
	 * Search this IP in each authors
	 * Can be long process.
	 * @return int $id_auteur
	 */
	public function search() : int {
		spip_timer('ips');
		$relations = ipset_get_all_ips_by_authors();
		include_spip('lib/IPSet/src/Wikimedia/IPSet');
		foreach ($relations as $id_auteur => $ips) {
			$ipset = new \Wikimedia\IPSet($ips);
			if ($ipset->match($this->ip)) {
				spip_log("Recherche IP $this->ip vers $id_auteur en " . spip_timer('ips'), 'ipset');
				return $id_auteur;
			}
		}
		spip_log("Recherche IP $this->ip inconnue en " . spip_timer('ips'), 'ipset');
		return 0;
	}

	/**
	 * Save in cache file the relation IP to id author.
	 * @param int $id_auteur
	 * @return bool
	 */
	public function save(int $id_auteur) : bool {
		sous_repertoire($this->dir_accessip);
		if (ecrire_fichier($this->dir_accessip . $this->ip, $id_auteur)) {
			@touch($this->dir_noflood . $this->ip);
			return true;
		}
		return false;
	}

	/**
	 * Remove cache files for this IP
	 */
	public function remove() {
		supprimer_fichier($this->dir_accessip . $this->ip);
		supprimer_fichier($this->dir_noflood . $this->ip);
	}

	/**
	 * Update timestamp of cache files for this IP
	 */
	public function touch() {
		@touch($this->dir_accessip . $this->ip);
		@touch($this->dir_noflood . $this->ip);
	}

}