<?php

include_spip('inc/ipset');

function ipset_ips_count($ip_ou_plage) {
	// Charger IPTools…
	if (!class_exists('\IPTools\Network')) {
		include_spip('lib/IPTools/src/PropertyTrait');
		include_spip('lib/IPTools/src/IP');
		include_spip('lib/IPTools/src/Range');
		include_spip('lib/IPTools/src/Network');
	}
	$network = \IPTools\Network::parse($ip_ou_plage);
	return $network->count();
}